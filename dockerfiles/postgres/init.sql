CREATE TABLE message
(
    id BIGINT PRIMARY KEY NOT NULL,
    message VARCHAR(255)
);

INSERT INTO public.message (id, message) VALUES (1, 'Message 1 in the Database');
INSERT INTO public.message (id, message) VALUES (2, 'Message 2 in the Database');