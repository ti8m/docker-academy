# Dockerfiles list

## docker-base-image

Installs the statically compiled docker binary from docker.com. This can be used as a base image in case docker is 
required inside an image.

This image demonstrates how to check the checksum for content downloaded within a Dockerfile 

## vncserver and wireshark

This show case demonstrates how to utilize wireshark to analyze the network traffic of a container. 

```sh
docker run -it -p 80:80 --name -d web nginx

docker run -it -p 5901:5901 --name vnc -d academy/vncserver

docker run -it --net=container:web --volumes-from vnc --cap-add NET_ADMIN -d academy/wireshark
```

Connect to the VNC server {docker host ip}:5901 

The password is set to 123456

## nodeserver
Node images which will run the docker-academy-slides. 
```
docker run --rm -it -p 9099:9099 --name my-nodeserver academy/nodeserver
```

##utility-image
Used to for tasks like volume backups and restore. Based on busybox.


