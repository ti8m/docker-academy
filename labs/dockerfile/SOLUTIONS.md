
# Solutions

### Task 1

https://hub.docker.com/_/postgres/ 


### Task 2

#### Building the image. 

```sh
FROM busybox:1.24.1
MAINTAINER ti8m docker academy

ENV DOCKER_ACADEMY "Hello from inside the container"

CMD echo $DOCKER_ACADEMY
```

Notice that we use the special format for the CMD instruction. The normal 
format would be `CMD ["executable", "param1"]` but this form does not 
interpolate ENV vars.

#### Starting the container

```sh
$ docker build -t lab-print .
$ docker run --rm -it lab-print
Hello from inside the container
$
```

The container will be stopped after the echo. Docker containers only run
as long as the startup process inside the container is running. In our 
case teh startup command is `echo`.

See also [Restart policies (--restart)](https://docs.docker.com/engine/reference/run/#restart-policies-restart) 
for possible option on how to handle container restarts.

### Task 3

#### Building the image

This Dockerfile has to be placed withing the maven module directory next
to the pom.xml file.

```sh
FROM centos:7.2.1511
MAINTAINER ti8m docker academy

RUN yum -y --setopt=tsflags=nodocs install \
        java-1.8.0-openjdk-headless \
        ca-certificates \
    && yum clean all

COPY target/springboot-frontend-1.0-SNAPSHOT.jar /app.jar

## make sure the file has a modified timestamp
RUN bash -c 'touch /app.jar'    

VOLUME /tmp

EXPOSE 8080

ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]
```

Notice that we also installed the ca-certificates package. This would 
not be required for our demo application but is very important for applications
which use https connections as it contains all trusted root certificates. 

#### Starting the container 

```sh
$ docker build -t <username>/academy-frontend:1.0 .
$ docker run -it --rm -p 8080:8080 <username>/academy-frontend:1.0
```

### Task 3.1

If we put the `Dockerfile` into the root folder of the maven module (recommended) 
the `.dockerignore` file could look like the following. The important part 
is the entry starting with `!`. We exclude everything in src and target 
except for jar file within the target folder. Keep in mind that without 
the `.dockerignore` file every file within in the maven module directory 
is sent to the docker daemon. 
 
```sh
src/
target/
!target/*.jar
```

### Task 3.2

This can be realized by defining the application startup command via
the `ENTRYPOINT` instructions and omitting `CMD`. That way additional parameters
can be passed in at container startup time via `docker run ... <image> arg1 arg2`.

### Optional Task 3.3

The user and group used are defined as ENV variables. This would not be 
required but increases the readability of the Dockerfile. We create the 
app group and app user within the `RUN` instruction. 

The `USER` instructions switches the user context in the Dockerfile. It 
is essentially the same as running the `su -` command.

```sh
FROM centos:7.2.1511
MAINTAINER ti8m docker academy

ENV APP_USER app
ENV APP_GROUP app

RUN groupadd -r -g 10000 $APP_GROUP \
    && useradd -r -g $APP_GROUP -u 10000 $APP_USER \
    && yum -y --setopt=tsflags=nodocs install \
        java-1.8.0-openjdk-headless \
        ca-certificates \
    && yum clean all

COPY target/springboot-frontend-1.0-SNAPSHOT.jar /app.jar

## make sure the file has a modified timestamp
RUN bash -c 'touch /app.jar'

USER $APP_USER

VOLUME /tmp

EXPOSE 8080

ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]
```

### Task 4

Tag and push the image.

```sh
$ docker tag <username>/academy-frontend:1.0 registry:5000/<username>/academy-frontend:1.0
$ docker push registry:5000/<username>/academy-frontend:1.0
```


### Make it smaller (alpine linux rocks)

```sh
FROM openjdk:8-jre-alpine
MAINTAINER ti8m docker academy

ENV APP_USER app
ENV APP_GROUP app

RUN adduser -D -u 10000 $APP_USER $APP_GROUP 

COPY target/springboot-frontend-1.0-SNAPSHOT.jar /app.jar

USER $APP_USER

VOLUME /tmp

EXPOSE 8080

ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]
```