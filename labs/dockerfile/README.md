
# Dockerfile lab

Keep in the mind that Docker provides a [Dockerfile best practices](https://docs.docker.com/engine/userguide/eng-image/dockerfile_best-practices/)
guide.

### Task 1

Explore the [Docker Hub](https://hub.docker.com/) and search for the 
official postgres image. Which versions (tags) are available? The official 
images go through extended testing and security scans. It is also a very 
good place to find good Dockerfiles. 

### Task 2

Create a simple Docker image which prints a string to the console 

The image shall:

- be based on busybox:1.24.1
- have the ENV var DOCKER_ACADEMY set
- print the value of the ENV var DOCKER_ACADEMY to stdout

Start a container and test if the expected value is printed on the console. 

What happens to the container after the message is printed? 

### Task 3

Create the docker image academy-fontend for the academy demo app 
(maven module springboot-frontend).

We assume you have already cloned the git repo and built the application 
with maven. A Spring Boot application packages all required dependencies
into a single executable jar file. Starting the app is as simple 
 as executing `java -jar /springboot.jar`. Therefore we only need to prepare 
the runtime environment (java) and copy the jar file into our image. In this 
lab we only cover the main application (springboot-frontend). The Docker 
image for the second Spring Boot application will be provided by us.  

The Dockerfile shall be placed into the folder `docker-academy-app/springboot-frontend`

The image shall:

- be based on centos:7.2.1511
- have the java runtime installed (package name:`java-1.8.0-openjdk-headless`)
- define /tmp as volume
- expose the Spring Boot default port 8080
- be tagged with <username>/academy-frontend:1.0

Packages are installed with the `yum` command. The flag `-y` is important
to perform a non interactive installation. As we want to keep the size 
of the container small we should also exclude the docs with the flag 
`--setopt=tsflags=nodocs`. 

Another best practice is to cleanup the yum cache within the same `RUN` 
instruction. Therefore you should always add `... && yum clean all` in case
you use yum in a `RUN` instruction. 


Complete command:

```sh
yum -y --setopt=tsflags=nodocs install <package> && yum clean all
```

The spring Boot application should be started with the command: 

```sh
java -Djava.security.egd=file:/dev/./urandom -jar /path/to/jar
```

Start the container in the foreground with the option `--rm` and map the 
container port 8080 to the host port 8080. 

Now you should be able to access the page http://10.0.0.100:8080/.

### Task 3.1

Create a .dockerignore file so that only the required file `target/*.jar` 
is sent to the Docker daemon.

### Task 3.2

Create the image so that it is possible to provide additional command 
line arguments while starting the container without the need to enter the
complete application start command.

### Optional Task 3.3

Prepare the Dockerfile so that the process inside the container does run
under the user `app`. 

> Hint: the user `app` has to be created within a RUN instruction. Best practice
would be to use a specific UID and GID for the user. 

The following commands create the group and the user with a specific UID 
and GID.

```sh
groupadd -r -g 10000 app
useradd -r -g app -u 10000 app
```

### Task 4

Push the image to our academy docker registry. 

> Hint: The registry DNS name inclusive port has to be part of the image tag. 

Our academy registry does not require authentication. You are able to push 
any image for any namespace. Therefore your username should always be part of the 
tag.