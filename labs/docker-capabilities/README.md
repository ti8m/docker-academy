
# Docker capabilities lab

In this lab we play around with the linux capabilities inside docker 
containers. The man page `man capabilities` contains all capabilities 
provided by the Linux kernel.

Docker enables only a subset of the available capabilities. The default 
capabilities can be found in the following source file [defaults_linux.go](https://github.com/docker/docker/blob/8d1d9eebe3ded3105216dc9a88cc94d16ac6198c/oci/defaults_linux.go#L62) and the full list is available in the file [capabilities_linux.go](https://github.com/docker/libcontainer/blob/master/capabilities_linux.go#L14)

With the docker command line arguments `--cap-drop` and `--cap-add` we can control the capabilities used for a specific container.

## Example:

```sh
docker run -it --rm --cap-drop NET_BIND_SERVICE busybox:1.24.1
/ # nc -l -p 80
nc: bind: Permission denied
```

## Task 1

Setting the time inside a container is not allowed with the default settings.

```sh
$ docker run --rm -it busybox:1.24.1
/ # date -s 09:41
date: can't set date: Operation not permitted
Thu Oct 27 09:41:00 UTC 2016
$ 
```

Find a way to set the system time inside a Docker container with the 
command ``date -s 09:41``. Use the same Docker image as shown in the 
example.
