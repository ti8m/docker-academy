# Capturing container network traffic with Wireshark

The docker compose file contains the full setup as shown in overview.png.
  
## Components

- nginx container which plays the role of the production application
- wireshark container which will reuse the network stack of the nginx container to be able to capture the network traffic. 
- vncserver container which provides the X11 funtionality and the VNC server to be able to run X11 applications remotely  

Build and run the containers with:

```sh
docker run -d -p 80:80 --name web nginx:1.9.11
docker run -d -p 5901:5901 --name vnc registry:5000/vnc
docker run --name wireshark --privileged=true --cap-add NET_ADMIN --volumes-from vnc -d --net container:web registry:5000/wireshark
```

The vnc server is available under 10.0.0.100:5901. You need a VNC client to be able to open the session. 

## VNC Client

The vnc password is: 123456

### Mac

Mac users are able to use the Finder which has VNC functionality. 

### Windows

Windows can use the java VNC client tightvnc-jviewer.jar within this lab directory.

```sh
java -jar tightvnc-jviewer.jar
```

## Capturing

Start capturing and access nginx on http://10.0.0.100:80. You should now
see the network traffic of the nginx container.

## Build images from Dockerfiles

The images can also be built from the following dockerfiles:

```sh
cd /vagrant/docker-academy/dockerfiles/vncserver
docker build -t vnc .

cd /vagrant/docker-academy/dockerfiles/wireshark
docker build -t wireshark .
```