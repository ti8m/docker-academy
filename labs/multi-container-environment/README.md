
# Multi container environments lab

In this lab we will explore the default Docker networking functionality.

First we will link two containers. This is consider a legacy method with 
Docker 1.10. The new networking feature will be used in the later tasks 
in this lab. CentOS/RedHat 7 only recently updated from Docker version 1.8.2 to 1.10.3.  

---

> Remember that Docker uses the container names for DNS lookups.

---
> Docker container names have to be unique. Therefore it might be that you
 have to delete a container if the name already exists. Deleting a container
 can be done with `docker rm <nameOrId>`

---

### Task 1

- Start an nginx:1.9.11 container with the name web1 without mapping any
ports
- Start a centos:7.2.1511 container in interactive mode (-it --rm) 

- Use `curl http://<container name>` to output the nginx welcome page to the 
console.

- Check the /etc/hosts file inside the centos container and try to fine the
entry which make our curl call work. 

### Task 2

- List all available networks
- Check the Docker host interfaces with the command `ip address`. 

> All interfaces except eth0, eth1 and lo belong to Docker.

- Create a network with the name `academy-net` 
- Check the docker host interfaces again with the command `ip address`. 
Which virtual interface belongs to the newly created network?
- Start an nginx:1.9.11 container with the name `web` and attach it to 
the just created network
- Start a centos:7.2.1511 container in interactive mode (-it --rm) and 
test the connectivity with curl.
- Check the /etc/hosts file inside the centos container. What is the 
difference to the linking example?
- Inspect the created network. Which IP address is assigned to the web container?

### Task 3

Our demo application contains an academy welcome service. The Docker image for
this microservice is available in the lab registry under `registry:5000/academy-welcome-service:1.0`.

Start the welcome service container with `--name academy-welcome-service` 
and attach it to the created network.

### Task 4

Use the Docker networking to connect the academy-frontend with a postgres
database. The postgres and academy-frontend containers shall be started the 
same way as you did it in the previous labs (inc. the same volume) but 
extended with the network assignment. The academy-frontend
image should already be available from the previous labs.

We need to let the academy-frontent container know where it can find the DB and the
welcome service. We inject this config iformation as as environment variables. 

- `DB_HOST=<container name>`
- `WELCOME_SVC_URL=http://<container name>:8080/welcome`

> Environment variables are injected with the `-e NAME=VALUE` flag of the docker 
run command.

Use the network `academy-net` which you created in the previous task. 

> Don't forget to map port 8080 to the docker host for the academy-frontend container

Open the academy demo app in the browser (http://10.0.0.100:8080) and check
id the db messages are displayed.
