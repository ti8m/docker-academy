# First steps lab

### Task 1

* start the image registry:5000/nodeserver:1.0 with the name docker-presentation and access the presentation. The default port is 9099 and we want to access the [presentation](http://10.0.0.100:9000/build/#/) on port 9000
* analyze the image and inspect the container
    * how was the presentation moved on the image?

### Task 2
During this course, we build, run and deploy a small spring boot application called academy demo app. In this task we build the maven application 
with the help of Docker. A JDK and Maven will not be installed on the lab vm.

* cd into `/vagrant/docker-academy-app`
* Build the application with the help of the maven:3.3.9-jdk-8 Docker image. 

We need to mount the application source code into the Docker container. This can be done with the option `-v $PWD:/app`. Details about 
this command will be covered later in the course. This would result in the following pom.xml path inside the 
container `/app/pom.xml`. The -f argument of mvn can be used to provide the path to the pom.xml.
