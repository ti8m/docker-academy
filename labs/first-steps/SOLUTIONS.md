# Solutions

### Task 1

```
$ docker run -d -p 9000:9099 --name docker-presentation registry:5000/nodeserver:1.0
$ docker inspect --format="{{.Config.WorkingDir}}" docker-presentation
```

Access the slides in the browser via http://10.0.0.100:9000/build/. 

### Task 2

```
$ cd /vagrant/docker-academy-app
$ docker run --rm -it -v $PWD:/app:  maven:3.3-jdk-8 mvn -f /app/pom.xml clean install

.....

[INFO] ------------------------------------------------------------------------
[INFO] Reactor Summary:
[INFO]
[INFO] docker-academy-app ................................. SUCCESS [  3.511 s]
[INFO] welcome-service-api ................................ SUCCESS [ 12.540 s]
[INFO] springboot-frontend ................................ SUCCESS [ 22.872 s]
[INFO] springboot-welcome-svc ............................. SUCCESS [  1.836 s]
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 42.913 s
[INFO] Finished at: 2016-10-27T08:04:14+00:00
[INFO] Final Memory: 33M/80M
[INFO] ------------------------------------------------------------------------
$ 
```
