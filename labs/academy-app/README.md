#Academy App
Compose file will start all relevant containers required for the academy app.

```
docker-compose up -d
```

## Graylog
Log management is handled with [graylog](https://www.graylog.org/ "Graylog Homepage"). You can access the logs can be access via the vagrant box IP
 [10.0.0.100:9000](http://10.0.0.100:9000/#fields=message%2Csource) username:admin password:admin
###Improvements / Ehancements
Not everything in this setup and in production graylog would be split into its components.

* Lookup via docker-network
 ```
 gelf-address: "udp://graylog:12201" 
 ```
 _Failed to initialize logging driver: gelf: cannot connect to GELF endpoint: graylog:12201 dial udp: lookup graylog on 10.0.2.3:53: server misbehaving._


* Create data volumes with compose and map them in the container volume section

 ```
 service:
   graylog:
     volume:
       - data:/var/opt/graylog/data:Z
 volumes:
   data:
 ```
  _Error executing action create on resource directory[/var/opt/graylog/data]_
